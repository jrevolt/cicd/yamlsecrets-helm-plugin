#!/bin/bash
set -eu

fail() {
  echo "[ERR] $@" >&2
  exit 1
}

# simple CLI parser
# usage:
# source <(args "$@")
# source <(args "--key1=value1" "--key2=value2")
args() {
  for i in "$@"; do
    echo -n "$i" | perl -E '
    $in = join("", <STDIN>);
    $k=$in; $k =~ s/^(?:--)?([^=]+)=.*$/$1/gms;
    $v=$in; $v =~ s/^[^=]+=(.*)$/$1/gms; $v =~ s/"/\\"/gms;
    say "local $k=\"$v\""'
  done
}

#which cygpath &>/dev/null &&
#realpath() {
#  cygpath -wam "$1"
#}

# ysecrets://[property=]path/to/file.yaml
# if $property is defined, whole YAML content is rewritten and assigned to defined property
# e.g. {foo: bar} => {property: {foo: bar}}
downloader() {
  local uri="$4"; uri=${uri//*:\/\//} # get uri, trim protocol://
  local file=${uri//*=/} # trim property= prefix, if any
  local property=""
  [[ "$uri" =~ .*=.* ]] && property=${uri//=*/} # extract property name

  local key=${YAMLSECRETS_KEY:-${HELM_SECRETS_KEY:-secrets.key}}
  [ -f ${file} ] || fail "Missing value file: ${4} => $(realpath $file)"
  [ -f ${key} ] || fail "Missing decryption key: \$YAMLSECRETS_KEY => $key => $(realpath $key)"
  if [[ "$property" != "" ]]; then
    yaml-secrets dec -y -k "$key" -f "$file" | yq -y "{$property: .}"
  else
    yaml-secrets dec -y -k "$key" -f "$file"
  fi
}

enc() {
  local key
  local file
  source <(args "$@")
  [[ $key ]]
  [[ $file ]]
  [[ -f $key ]] && local keyfile=$key
  echo "Encrypting ${file} using key ${keyfile:-inline}"
  yaml-secrets enc -y -k "$key" -f "$file" -o "$file"
}

dec() {
  local key
  local file
  source <(args "$@")
  [[ $key ]]
  [[ $file ]]
  [[ -f $key ]] && local keyfile=$key
  file=${file//*=/}
  echo "Decrypting ${file} using key ${keyfile:-inline}"
  yaml-secrets dec -y -k "$key" -f "$file" -o "$file"
}


"$@"

