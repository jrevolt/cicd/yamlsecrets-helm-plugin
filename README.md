# yamlsecrets-helm-plugin

## usage

```shell
helm plugin install https://gitlab.com/jrevolt/cicd/yamlsecrets-helm-plugin.git --version 1.0.0
dotnet tool install JRevolt.YamlSecrets.Cli --version="1.*"
```

```shell
yaml-secrets keygen -i /path/to/secrets.key
export YAMLSECRETS_KEY=/path/to/secrets.key
```

values.yaml
```yaml
---
username: johndoe
password: !<secret> topsecret
```

```shell
yaml-secrets enc -y -f values.yaml -k secrets.pub -i
```

```shell
helm template -g . -f ys://values.yaml
```

